package ru.bearing.bearingssite.Service;

import ru.bearing.bearingssite.Entity.User;

import java.util.List;

public interface UserService {
    List<User> findAll();

    void add(User user);
}
