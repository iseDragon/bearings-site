package ru.bearing.bearingssite.Controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.bearing.bearingssite.Entity.User;
import ru.bearing.bearingssite.Service.UserService;

import java.util.List;

@RestController
@RequestMapping("/users")
public class UserController {

    private final UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping
    public List<User> getAll() {
        return  userService.findAll();
    }
    @PostMapping
    public void  add(@RequestBody User user) {
        userService.add(user);
    }
}
