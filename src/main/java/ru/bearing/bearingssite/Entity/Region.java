package ru.bearing.bearingssite.Entity;

import lombok.Data;
import lombok.ToString;

import javax.persistence.*;
import java.util.Set;

@Data
@Entity
@ToString(exclude = "addresses")
public class Region {

    @Id
    @GeneratedValue
    private int id;
    private  String region;
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "region")
    private Set<Address> addresses;

}
