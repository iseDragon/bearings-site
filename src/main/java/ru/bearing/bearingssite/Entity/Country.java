package ru.bearing.bearingssite.Entity;

import lombok.Data;
import lombok.ToString;

import javax.persistence.*;
import java.util.Set;

@Data
@Entity
@ToString(exclude = "addresses")
public class Country {

    @Id
    @GeneratedValue
    private int id;
    private  String country;
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "country")
    private Set<Address> addresses;
}