package ru.bearing.bearingssite.Entity;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@Entity
public class CatalogItems {

    @Id
    @GeneratedValue
    private int id;
    private String originalNameNospaces;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn
    private NameItems biaringName;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn
    private NameItems originalName;
    private int groupId;
    private int drawingId;
    private int groupItemId;
    private String paramInnerDiam;
    private String paramOuterDiam;
    private String paramWidth;
    private String paramT;
    private String paramC;
    private byte moderated;
    private byte exeption;
    private byte dontDelete;
    private String undefinedVariations;

}
