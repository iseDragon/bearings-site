package ru.bearing.bearingssite.Entity;

import lombok.Data;
import lombok.ToString;

import javax.persistence.*;
import java.util.Set;

@Data
@ToString(exclude = "addresses")
@Entity
public class City {

    @Id
    @GeneratedValue
    private int id;
    private String city;
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "city")
    private Set<Address> addresses;
}


