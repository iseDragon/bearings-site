package ru.bearing.bearingssite.Entity;

import lombok.Data;
import lombok.ToString;

import javax.persistence.*;
import java.util.Set;


@Data
@Entity
@ToString(exclude = {"addresses", "address"})
public class NameItems {
    @Id
    @GeneratedValue
    private  int id;
    private String Name;
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "biaringName")
    private Set<CatalogItems> addresses;
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "originalName")
    private Set<CatalogItems> address;
}
