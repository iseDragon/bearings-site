package ru.bearing.bearingssite.Entity;

import lombok.Data;
import lombok.ToString;

import javax.persistence.*;
import java.util.Set;

@Data
@Entity
@ToString(exclude = "addresses")
public class Avenue {

    @Id
    @GeneratedValue
    private int id;
    private String avenue;
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "avenue")
    private Set<Address> addresses;
}
