package ru.bearing.bearingssite.Entity;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;
import java.util.List;
import java.util.Set;

@Data
@Entity
public class Provider {

    @Id
    @GeneratedValue
    private int id;
    private String name;

    @ManyToMany
    @JoinTable
//            (name = "HAS",
//            joinColumns = @JoinColumn(name = "AUTHOR_ID", referencedColumnName = "ID"),
//            inverseJoinColumns = @JoinColumn(name = "BOOK_ID", referencedColumnName = "ID"))
    private Set<Phone> phones;
    private String email;
    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn
    private Address address;
    private int status;
    private int access;
    private Date last_upload;
}
