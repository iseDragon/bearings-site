package ru.bearing.bearingssite.Entity;

import lombok.Data;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.Date;

@Data
@Entity
public class Banners {

    @Id
    @GeneratedValue
    private int id;
    private String name;
    private String advertiserEmail;
    private String path;
    private String url;
    private int clickCounts;
    private Date showStart;
    private Date showEnd;
}
