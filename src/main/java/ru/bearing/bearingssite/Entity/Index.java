package ru.bearing.bearingssite.Entity;

import lombok.Data;
import lombok.ToString;


import javax.persistence.*;
import java.util.Set;

@Data
@ToString(exclude = "addresses")
@Entity
public class Index {

    @Id
    @GeneratedValue
    private int id;
    private long index;
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "index")
    private Set<Address> addresses;

}
