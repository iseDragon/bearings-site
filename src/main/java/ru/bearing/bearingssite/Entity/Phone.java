package ru.bearing.bearingssite.Entity;


import lombok.Data;
import lombok.ToString;
import javax.persistence.*;
import java.util.Set;

@Data
@Entity
@ToString(exclude = "providers")
public class Phone {

    @Id
    @GeneratedValue
    private int id;
    private String code;
    private long number;

    @ManyToMany
    @JoinTable
//            (name = "HAS",
//            joinColumns = @JoinColumn(name = "AUTHOR_ID", referencedColumnName = "ID"),
//            inverseJoinColumns = @JoinColumn(name = "BOOK_ID", referencedColumnName = "ID"))
    private Set<Provider> providers;

}
