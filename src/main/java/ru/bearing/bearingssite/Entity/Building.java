package ru.bearing.bearingssite.Entity;

import lombok.Data;
import lombok.ToString;
import javax.persistence.*;
import java.util.Set;

@Data
@Entity
@ToString(exclude = "addresses")
public class Building {

    @Id
    @GeneratedValue
    private int id;
    private String building;
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "building")
    private Set<Address> addresses;
}
