package ru.bearing.bearingssite;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BearingsSiteApplication {

    public static void main(String[] args) {
        SpringApplication.run(BearingsSiteApplication.class, args);
    }
}
