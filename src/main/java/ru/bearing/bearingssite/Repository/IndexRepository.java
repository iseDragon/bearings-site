package ru.bearing.bearingssite.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.bearing.bearingssite.Entity.Index;

@Repository
public interface IndexRepository extends JpaRepository<Index, Integer> {
}
