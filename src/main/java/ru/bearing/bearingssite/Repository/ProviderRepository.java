package ru.bearing.bearingssite.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.bearing.bearingssite.Entity.Provider;

@Repository
public interface ProviderRepository extends JpaRepository<Provider, Integer> {
}
