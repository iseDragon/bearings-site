package ru.bearing.bearingssite.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.bearing.bearingssite.Entity.CatalogItems;


@Repository public interface CatalogItemsRepository extends JpaRepository<CatalogItems, Integer> {
}
