package ru.bearing.bearingssite.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.bearing.bearingssite.Entity.Avenue;

@Repository
public interface AvenueRepository extends JpaRepository<Avenue, Integer> {
}
