package ru.bearing.bearingssite.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.bearing.bearingssite.Entity.Phone;

@Repository
public interface PhoneRepository extends JpaRepository<Phone,Integer> {
}
