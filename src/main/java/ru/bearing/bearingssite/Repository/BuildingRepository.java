package ru.bearing.bearingssite.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.bearing.bearingssite.Entity.Building;

@Repository
public interface BuildingRepository extends JpaRepository<Building, Integer> {
}
