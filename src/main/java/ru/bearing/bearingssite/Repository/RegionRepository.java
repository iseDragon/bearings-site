package ru.bearing.bearingssite.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.bearing.bearingssite.Entity.Region;

@Repository
public interface RegionRepository extends JpaRepository<Region, Integer> {
}
