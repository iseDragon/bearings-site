package ru.bearing.bearingssite.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.bearing.bearingssite.Entity.Address;

@Repository
public interface AddressRepository extends JpaRepository<Address,Integer> {
}
