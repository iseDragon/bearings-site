package ru.bearing.bearingssite.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.bearing.bearingssite.Entity.City;

@Repository
public interface CityRepository extends JpaRepository<City,Integer> {
}
