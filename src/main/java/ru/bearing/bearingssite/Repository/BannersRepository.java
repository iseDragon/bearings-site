package ru.bearing.bearingssite.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.bearing.bearingssite.Entity.Banners;

@Repository
public interface BannersRepository extends JpaRepository<Banners, Integer> {
}
