package ru.bearing.bearingssite.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.bearing.bearingssite.Entity.Country;

@Repository
public interface CountryRepository extends JpaRepository<Country, Integer> {
}
