package ru.bearing.bearingssite.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.bearing.bearingssite.Entity.NameItems;

@Repository
public interface NameItemsRepository extends JpaRepository<NameItems, Integer> {
}
