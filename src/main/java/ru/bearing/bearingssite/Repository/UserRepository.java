package ru.bearing.bearingssite.Repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.bearing.bearingssite.Entity.User;

@Repository
public interface UserRepository extends JpaRepository <User, Integer> {
}
